let productBin = {
    "requestId": null,
    "data": [
      {
        "id": 100000057465,
        "storageId": 10000008207,
        "code": "A01-01-01-A",
        "productId": 110000081009,
        "productName": "FloBrand-DressBSPink",
        "productCode": "FBR00040101",
        "quantity": 76,
        "createdTime": "2021-12-21T13:54:48Z",
      },
      {
        "id": 100000057466,
        "storageId": 10000002181,
        "code": "A01-01-01-B",
        "productId": 110000081009,
        "productName": "FloBrand-DressBSPink",
        "productCode": "FBR00040101",
        "quantity": 71,
        "createdTime": "2021-12-21T13:54:48Z",
      },
      {
        "id": 100000065224,
        "storageId": 10000008884,
        "code": "Tgt00-A-A-01",
        "productId": 110000081009,
        "productName": "FloBrand-DressBSPink",
        "productCode": "FBR00040101",
        "quantity": 10,
        "createdTime": "2022-02-08T10:35:19Z",
      }
    ],
    "message": "success"
  }
 

/* 
// Assuming you have the productBin object

// Accessing the data array
const dataArray = productBin.data;

// Iterating over each object in the data array
dataArray.forEach(item => {
    // Accessing the productCode property of each object
    const productCode = item.productCode;
    console.log(productCode);
});

dataArray.forEach(item => {
  // Accessing the productCode property of each object
  const quantity = item.quantity;
  console.log(quantity);
});
*/

// Assuming you have the productBin object

// Accessing the data array
const dataArray = productBin.data;

// Initializing sum
let sum = 0;

// Iterating over each object in the data array
dataArray.forEach(item => {
    // Checking if the productCode matches the desired code
    if (item.productCode === "FBR00040101") {
        // Adding the quantity to the sum
        sum += item.quantity;
    }
});

// Logging the sum
console.log("Total quantity for productCode FBR00040101:", sum);


